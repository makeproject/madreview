import { AppRegistry } from 'react-native';
import App from './App';
import SplashScreen from './app/components/SplashScreen'; 
import MainApp from './app/components/MainApp';
import {
    StackNavigator,
} from 'react-navigation';
import Rate from './app/components/Rate';

const appScreen = StackNavigator({
    Main: {screen: MainApp},
    Rate: {screen: Rate}
})
AppRegistry.registerComponent('MadRate', () => appScreen);

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,StatusBar, Image,TouchableOpacity
} from 'react-native';


export default class RateItem extends Component{
        
        constructor(props){
            super(props);
            this.state = {itemName:'item'}
            
            
        }
        show=()=>{
            alert(this.props.text);
        }
        
        render(){
            let display = this.props.text;
            let resource = this.props.img == ''?'../img/medal.png':this.props.img;
            
            return(
                
                <TouchableOpacity style={styles.container} 
                    onPress={this.props.onClick}>
                    <View style={{alignItems:'center'}}>
                        <Image source={this.props.img} style={styles.img}/>
                        <Text style={styles.itemText}>{display}</Text>
                    </View>
                </TouchableOpacity>
                
            )
        }
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        paddingTop:20,
        paddingBottom:20,
        alignItems:'center',
        marginTop:0,
        backgroundColor:'#FFF',
        borderColor:'#f6f6f6',
        borderWidth:1,
        margin:0,
        justifyContent:'center'
    },
    img:{
        width:30,
        height:30,
        
        tintColor:'#e3211a'
    },
    itemText:{
        marginTop:5,
        color:'#e3211a',
        fontSize:10
    }
});

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,StatusBar, Image
} from 'react-native';
import App from '../../App';

const platformTitle = "You may need a title mad to change the world. So let's go rating us.";
export default class SplashScreen extends Component{
    
    constructor(props) {
        super(props);
        this.state = {isSplashScreen: true};
        

    }

    render(){
        return(
            
            <View style={styles.container}>
                <StatusBar barStyle="light-content" backgroundColor="#e3211a"/>
                <Image source={require('../img/mad_logo.png')}
                style={styles.logoStyle}/>
                <Text style={styles.title}>{platformTitle}</Text>
            </View>
        );
    }
}

const styles =StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#e3211a',
    },
    title: {
        textAlign: 'center',
        color: '#333333',
        color:"#FFF",
        fontSize:15,
        paddingLeft:20,
        paddingRight:20
    },
    logoStyle:{
        width: 200,
        height: 70,
        marginBottom: 20
    }
});

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ScrollView, Image,StatusBar, StatusBarIOS
  ,TouchableOpacity,
  BackHandler,
  BackAndroid,
  TextInput,KeyboardAvoidingView, Alert,Keyboard, NetInfo, ProgressViewIOS
} from 'react-native';
import  Rating from 'react-native-easy-rating';
import { ifIphoneX } from 'react-native-iphone-x-helper';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import NoConnection from './NoConnection';
import { ProgressDialog } from 'react-native-simple-dialogs';
import LoadingView from 'react-native-loading-view';
import Hyperlink from 'react-native-hyperlink';
const slectedColor ='red';

export default class Rate extends Component{
    
    constructor(props){
        super(props);
        this.state = {
            rateCount:0,
            isClient:true,
            description:'',
            isProgress:false,
            showView:'0%',
            successfullyDisplay:'none'
        }
        
    }
    componentDidMount(){
       
    }
  
   
    static navigationOptions = {
        header: null
    };
    post = ()=>{
        Keyboard.dismiss;
        if(this.state.rateCount==0){
            Alert.alert(
                'Invalid',
                'Please at-least rate us one star to go.'
            )
        }else if(this.state.description==''){
           Alert.alert(
                'Confirmation',
                'Are you sure you want to submit rating without description?',
                [
                    {text:'Yes', onPress:this.submitRate},
                    {text:'No', onPress:()=>this.inputDescription.focus()}
                ]
           );
        }else{
            this.submit();
        }
        
    }
    cleanUi(){
        
    }
    submitRate = ()=>{
        this.submit();
    }
    
    submit(){
        this.setState({showView:'100%'});
        this.setState({isProgress:true});
        let urlHeader= "http://192.168.1.101:8080/api/rate_madv2?star="+this.state.rateCount
            +"&review_message="+this.state.description
            +"&rater_type="+(this.state.isClient?"Client":"Employee")
            +"&review_category="+this.props.navigation.state.params.name;
        fetch(urlHeader)
            .then((res)=>res.json())
            .then((json)=>{

                this.setState({showView:'0%'});
                this.setState({isProgress:false});
                if(json.header.result){
                    this.setState({successfullyDisplay:'flex'})
                }else{
                    alert('An occurred error :(');
                }

            }).catch((err)=>{
                this.setState({showView:'0%'});
                this.setState({isProgress:false});
                alert('Error relate to internet, please try again later.');
            })
    }
    
        
    
    
    render(){
        return(
            <View width='100%'
                height='100%'
                style={{flex:1, justifyContent:'flex-end'}}>
                <View style= {styles.container}>
                    
                    <View style={styles.status}/>
                    <View style={styles.toolBar}>
                        <TouchableOpacity onPress={
                            ()=>this.props.navigation.goBack()
                        }>
                            <View style={styles.backLayout}>
                                <Image
                                    source={require('../img/back.png')} 
                                    style={styles.backImg}/>
                                <Text
                                    style={{color:'#FFF', paddingLeft:10}}>BACK
                                </Text>
                                
                            </View>
                        </TouchableOpacity>
                        <View style={{flex:1,padding:10,alignItems:'flex-end'}}>
                            <TouchableOpacity onPress={this.post}>
                                <Text
                                    style={{color:"#FFF", fontWeight:"bold", padding:10}}>
                                    POST</Text>
                            </TouchableOpacity>
                            
                        </View>
                        
                    </View>
                    <ScrollView>
                        <KeyboardAwareScrollView>
                            <View style={styles.rateContainer}>
                                
                                <Text
                                    style={{fontSize:20,paddingBottom:10,
                                    color:'black'}} onPress={()=>this.setState({isClient:false})}>
                                    How was {this.props.navigation.state.params.name}?
                                </Text>

                                
                                
                                <Rating 
                                        rating={this.state.rateCount}
                                        max={5}
                                        iconWidth={30}
                                        iconHeight={30}
                                        iconSelected={require('../img/star.png')}
                                        iconUnselected={require('../img/un_star.png')}
                                    
                                        onRate={(rating)=>this.setState({rateCount:rating})}/>
                                        
                                        <View width='100%'>
                                            <TouchableOpacity style={{flex:1}}
                                                onPress={()=>this.setState({isClient:true})}>
                                                <View
                                                    width='100%' 
                                                    style={{borderColor:'#EEE', 
                                                        borderBottomWidth:1,
                                                        borderTopWidth:1,marginTop:20,
                                                        padding:15, flexDirection:'row', justifyContent:'center',alignItems:'center'}}>
                                                
                                                    <Image
                                                        style={{width:24, height:24, 
                                                        tintColor:this.state.isClient?'#e3211a':'#7f7f7f'}}  
                                                        source={require('../img/employees.png')}/>
                                                    <Text style={{flex:1, paddingLeft:20, 
                                                        fontWeight:this.state.isClient?'bold':'normal',
                                                        color:this.state.isClient?'#e3211a':'#7f7f7f'}}>Rate as client</Text>
                                                    
                                                </View>
                                            </TouchableOpacity>
                                        </View>

                                        <View width='100%'>
                                            <TouchableOpacity style={{flex:1}}
                                            onPress={()=>this.setState({isClient:false})}>
                                                <View
                                                    width='100%' 
                                                    style={{borderColor:'#EEE', 
                                                        borderBottomWidth:1,
                                                        borderTopWidth:0,marginTop:0,
                                                        padding:15, flexDirection:'row', justifyContent:'center',alignItems:'center'}}>
                                                
                                                    <Image
                                                        style={{width:24, height:24,
                                                        tintColor:this.state.isClient?'#7f7f7f':'#e3211a'}}  
                                                        source={require('../img/support.png')}/>
                                                    <Text style={{flex:1, paddingLeft:20, 
                                                        fontWeight:this.state.isClient?'normal':'bold',
                                                        color:this.state.isClient?'#7f7f7f':'#e3211a'}}>Rate as employee</Text>
                                                    
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                        
                                        
                                        

                                        <TextInput style={styles.input} width = "100%" 
                                        placeholder='Review description'
                                        multiline={true}
                                        value={this.state.description}
                                        ref={(input)=>this.inputDescription=input}
                                        onChangeText={(description)=>this.setState({description:description})}
                                        underlineColorAndroid='rgba(0,0,0,0)'
                                        autoCapitalize="none"
                                        tintColor={"green"}
                                        placeholderTextColor='#acacac'/>
                            </View>
                        </KeyboardAwareScrollView>
                        
                        
                        
                    </ScrollView>
                
                </View>
                <View width={this.state.showView}
                    height={this.state.showView} visible={false}
                    style={{position:'absolute',top:0,
                        backgroundColor:'rgba(250,250,250,0.8)',
                        justifyContent:'center',alignItems:'center'}}>
                    
                    <LoadingView loading={true}/>
                    <Text style={{position:'absolute',paddingTop:40}}>{this.state.isProgress?'Sending...':''}</Text>
        
                </View>
                <View width='100%' height='100%'
                    style={{
                        position:'absolute',
                        top:0,
                        backgroundColor:'rgba(250,250,250,0.9)',
                        justifyContent:'center',
                        alignItems:'center',
                        padding:40,
                        display:this.state.successfullyDisplay
                    }}>
                    <Image style={{width:80,height:80,tintColor:'#e3211a'}} source={require('../img/high-five.png')}/>
                    <Text style={{textAlign:'center',paddingTop:10,color:'#e3211a'}}>{"Thanks for keeping in touch with us. Your review is now sent us."}</Text>

                    <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
                        <Text style={styles.roundButton}>Make new review</Text>                                   
                    </TouchableOpacity>
                    
                </View>
            </View>
            
        );
    }
}
const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#FFF'
    },
    status:{
        ...ifIphoneX({
            height: 35
        }, {
            height:Platform.select({ios:20,android:0}),
        }), 
      
      backgroundColor:"#e3211a",
    },
    toolBar:{
        height:50,
        backgroundColor: '#e3211a',
        alignItems:'center',
        elevation:3,
        flexDirection:'row'

    },
    backImg:{
        width:20,
        height:20,
        tintColor:'#fff'
    },
    backLayout:{
        flexDirection:'row',
        alignItems:'center',
        paddingLeft:10       
    },
    rateContainer:{
        flex: 1,
        alignItems:'center',
        justifyContent:'center',
        marginTop:50
       
    },
    input: {
        
        justifyContent: "center",
        padding:20,
        color:'#000',
        borderRadius: 5,
        marginTop: 20,
        fontSize:20
      }
      ,retryLayout:{
          position:'absolute',
          top:0,
          backgroundColor:'rgba(250,250,250,0.9)',
          justifyContent:'center',
          alignItems:'center',
          padding:40,
          display:'none'
      },
      roundButton:{
          paddingLeft:20,
          paddingRight:20,
          paddingTop:10,
          paddingBottom:10,
          borderColor:'#e3211a',
          borderWidth:2,
          borderRadius:20,
          marginTop:20,
          color:'#e3211a',
          fontWeight:'bold'
      }
})

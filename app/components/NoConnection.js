import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,StatusBar, Image,Button, ScrollView,KeyboardAvoidingView, TouchableOpacity
} from 'react-native';

export default class NoConnection extends Component{
    render(){
        return(
            <View style={{
                    backgroundColor:'#e3211a',
                    flex:1,
                    alignItems:'center',
                    justifyContent:'center'
                }}>
                <Image style={styles.img} source={require('../img/wifi.png')}/>
                <Text style={styles.textStyle}>Oops! No connection avialable. you will need connection to be rated.</Text>
                <TouchableOpacity>

                </TouchableOpacity>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    img:{
        width: 150,
        height: 150,
        tintColor: '#FFF'
    },
    textStyle:{
        color:'#FFF',
        textAlign:'center',
        marginTop:10,
        paddingLeft:60,
        paddingRight:60
    }
})
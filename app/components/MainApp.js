import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,StatusBar, Image,Button, ScrollView,KeyboardAvoidingView, TouchableOpacity
} from 'react-native';
import SplashScreen from './SplashScreen';
import App from '../../App';
import Rate from './Rate';
import RateItem from '../components/RateItem';
import { ifIphoneX } from 'react-native-iphone-x-helper';
import Hyperlink from 'react-native-hyperlink';
export default class MainApp extends Component{
    constructor(props) {
        super(props);
        this.state = {
                        isSplashScreen: true
                    };
        setTimeout(v=>this.setState({isSplashScreen:false}),3000)

    }
    static navigationOptions = {
        title: 'Home',
        header: null
      };

    render(){
        
        return(
            
            <View style={styles.container}>
                <StatusBar barStyle='light-content' backgroundColor="#c3140e"/>
                {this.state.isSplashScreen?
                <View style={styles.splashScreenStyle}>
                    <Image style={styles.logo} source={require('../img/mad_logo.png')}/>
                    <Text
                        style={{color:'#FFF', paddingLeft:40,paddingRight:40,textAlign:'center'}}>
                        {"You may need a title mad to change the world. So let's go rating us."}
                    </Text>
                </View>
                :<View style={styles.container}>
                    <View style={styles.status}/>
                    <View style={styles.toolBar}>
                        <Text style={styles.appTitle}>Rete for Mäd</Text>
                    </View>
                    <ScrollView >
                        <View style={{borderColor:'#f6f6f6', borderWidth:1, margin:5}}>
                            <View style={styles.gridRow}>
                                <View style={{flex:1}}>
                                    <RateItem 
                                        text='Creative' 
                                        img={require('../img/creative-process.png')}
                                        onClick={()=>this.props.navigation.navigate('Rate', {name:"Creative"})}
                                        />
                                    </View>  
                                    <View style={{flex:1}}>
                                    <RateItem 
                                        text='Speed' 
                                        img={require('../img/urgency.png')}
                                        onClick={()=>this.props.navigation.navigate('Rate', {name:"Speed"})}/>
                                        
                                    </View>  
                                    <View style={{flex:1}}>
                                    <RateItem 
                                        text='Value' 
                                        img={require('../img/increase.png')}
                                        onClick={()=>this.props.navigation.navigate('Rate', {name:"Value"})}
                                        />
                                    </View>  
                            </View>
                            <View style={styles.gridRow}>
                                <View style={{flex:1}}>
                                    <RateItem 
                                        text='Quality' 
                                        img={require('../img/medal.png')}
                                        onClick={()=>this.props.navigation.navigate('Rate', {name:"Quality"})}
                                        />
                                    </View>  
                                    <View style={{flex:1}}>
                                    <RateItem 
                                        text='Strategy' 
                                        img={require('../img/brainstorming.png')}
                                        onClick={()=>this.props.navigation.navigate('Rate', {name:"Strategy"})}/>
                                    </View>  
                                    <View style={{flex:1}}>
                                    <RateItem 
                                        text='Other' 
                                        img={require('../img/other.png')}
                                        onClick={()=>this.props.navigation.navigate('Rate',
                                        {name:'Other'})}/>
                                    </View>  
                            </View>
                        </View>
                        <View style={styles.description}>
                            <Text style={styles.subTitle}>Description</Text>
                            <Hyperlink linkDefault={true}>
                                <Text style={styles.textDescription}>{textDescription}</Text>
                                
                                <Text style={styles.textDescription}>{"\nvisit us at"}</Text>
                                <TouchableOpacity>
                                    <Text>website: www.mäd.com</Text>
                                </TouchableOpacity>    
                            </Hyperlink>
                        </View>
                    </ScrollView>
                </View>}
                
            </View>
        )
    }

}
const mailsTo="";
const textDescription = "To reviewing us you will need picked an items up there and giving us the star from 1 to 5. "
+"And your ratting will let's other people and us to know our performent."+
"";

const styles=StyleSheet.create({
    container:{
        backgroundColor: '#FFF',
        flex:1
    },
    logo:{
        width: 200,
        height:70
    },
    categoryItem:{
        flex:1,
        backgroundColor: '#FFF',
        margin:0,
        padding: 10,
        borderWidth:1,
        borderColor:'#EEE',
        justifyContent:'center',
        alignItems:'center'
    },
    status:{
        ...ifIphoneX({
            height: 35
        }, {
            height:Platform.select({ios:20,android:0}),
        }), 
      
      backgroundColor:"#e3211a",
    },
    imgBox:{
      width:50,
      height:50
    },
    toolBar:{
      height:50,
      backgroundColor: '#e3211a',
      justifyContent: 'center',
      elevation:3
      
    },
    appTitle:{
      color:'#FFF',
      fontSize:16,
      fontWeight:'bold',
      paddingLeft: 15
    },
    gridRow: {
      flexDirection: 'row'
    },
    boldText:{
      color: '#000',
      fontWeight:'bold'
    },
    subTitle:{ 
  
      fontWeight:'bold',color:'#000',fontSize:15,paddingBottom:10
    },
    description:{
      padding: 20
    },
    textDescription:{
      color:'#757575',
      justifyContent:'flex-end'
    },
    splashScreenStyle: {
        flex:1,
        backgroundColor: '#c3140e',
        alignItems:'center',
        justifyContent:'center'
    },
    link:{
    
        color: '#3181e3',
        paddingTop:10,paddingLeft:0
        
    }
});
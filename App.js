/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ScrollView, Image,StatusBar, StatusBarIOS
  ,TouchableOpacity,
  BackHandler,AppRegistry, Modal
} from 'react-native';
import {
  StackNavigator,
} from 'react-navigation';


import RateItem from './app/components/RateItem';
import Rate from './app/components/Rate';
import Hyperlink from 'react-native-hyperlink';


const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

class RateToMad extends Component{
  render(){
    return(
      <View>
        <Rate/>
      </View>
    );
  }
}

export default class App extends Component {
  static navigationOptions = {
    title:'Home'
  }  
  constructor(props){
    super(props);
    this.state = {isShowModal:false}
  }
  next=()=>{
    
  }
  render() {
   
    
    return (
        
        <View style={{flex:1, backgroundColor:'#FFF'}}>
          
          <StatusBar barStyle='light-content' backgroundColor='#c3140e'/>
          
          <View style={styles.status}/>
          <View style={styles.toolBar}>
              <Text style={styles.appTitle}>Rate for Mäd</Text>
          </View>
          <ScrollView style={{backgroundColor:'#FFF'}}>
            
            <View style={{borderColor:'#f6f6f6', borderWidth:1, margin:5}}>
              <View style={styles.gridRow}>
                <View style={{flex:1}}>
                  <RateItem 
                    text='Creative' 
                    img={require('./app/img/creative-process.png')}/>
                </View>  
                <View style={{flex:1}}>
                  <RateItem 
                    text='Speed' 
                    img={require('./app/img/urgency.png')}/>
                </View>  
                <View style={{flex:1}}>
                  <RateItem 
                    text='Value' 
                    img={require('./app/img/increase.png')}/>
                </View>  
              </View>

              <View style={styles.gridRow}>
                <View style={{flex:1}}>
                  <RateItem 
                    text='Quality' 
                    img={require('./app/img/medal.png')}/>
                </View>  
                <View style={{flex:1}}>
                  <RateItem 
                    text='Strategy' 
                    img={require('./app/img/brainstorming.png')}/>
                </View>  
                <View style={{flex:1}}>
                  <RateItem 
                    text='Other' 
                    img={require('./app/img/other.png')}
                    onClick={this.next}/>
                </View>  
              </View>

            </View>      

            <View style={styles.description}>
                <Text style={styles.subTitle}>Description</Text>
                <Hyperlink>
                  <Text style={styles.textDescription}>{textDescription}</Text>
                </Hyperlink>
                
            </View>
          </ScrollView>
        </View>
    );
  }
}
const textDescription = "To ratting us you will picked an items up there and giving us the star from 1 to 5. "
+"And your ratting will let's other people and us to know our performent.\n\nThanks for your rate."+
"Your review will send to: dev.rithyuy@gmail.com";

const styles = StyleSheet.create({
  container: {
    flexDirection:'row',
    backgroundColor: '#FFF',
    marginTop:0
  },
  categoryItem:{
      flex:1,
      backgroundColor: '#FFF',
      margin:0,
      padding: 10,
      borderWidth:1,
      borderColor:'#EEE',
      justifyContent:'center',
      alignItems:'center'
  },
  status:{
    height:Platform.select({ios:20,android:0}),
    backgroundColor:"#c3140e",
  },
  imgBox:{
    width:50,
    height:50
  },
  toolBar:{
    height:50,
    backgroundColor: '#e3211a',
    justifyContent: 'center',
    elevation:3
    
  },
  appTitle:{
    color:'#FFF',
    fontSize:16,
    fontWeight:'bold',
    paddingLeft: 15
  },
  gridRow: {
    flexDirection: 'row'
  },
  boldText:{
    color: '#000',
    fontWeight:'bold'
  },
  subTitle:{ 

    fontWeight:'bold',color:'#000',fontSize:15,paddingBottom:10
  },
  description:{
    padding: 20
  },
  textDescription:{
    color:'#878787'
  }
  
});

const myscreen = StackNavigator({
  Home: {
    screen: App
  },
  Detail:{
    screen: Rate
  }
})
AppRegistry.registerComponent('MadRate', () => App);
